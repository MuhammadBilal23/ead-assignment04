$(document).ready(function(){

var d = new Date();


function myCalendar() {

var month = d.getUTCMonth();
var day = d.getUTCDate();
var year = d.getUTCFullYear();
var nextMonth = month + 1;
var preMonth = month - 1;
var febDays = '';
var html = '';
var counter = 1;







// Displays the current month in Strings and the actual year 
switch(month) {
	case 0: $('.month-year').append('<h3> ' + 'January' + ' ' +  year + ' </h3>' ); break;
	case 1: $('.month-year').append('<h3> ' + 'February' + ' ' + year + ' </h3>' ); break;
	case 2: $('.month-year').append('<h3> ' + 'March' + ' ' + year + ' </h3>' ); break;
	case 3: $('.month-year').append('<h3> ' + 'April' + ' ' + year + ' </h3>' ); break;
	case 4: $('.month-year').append('<h3> ' + 'May' + ' ' + year + ' </h3>' ); break;
	case 5: $('.month-year').append('<h3> ' + 'June' + ' ' + year + ' </h3>' ); break;
	case 6: $('.month-year').append('<h3> ' + 'July' + ' ' + year + ' </h3>' ); break;
	case 7: $('.month-year').append('<h3> ' + 'August' + ' ' + year + ' </h3>' ); break;
	case 8: $('.month-year').append('<h3> ' + 'September' + ' ' + year + ' </h3>' ); break;
	case 9: $('.month-year').append('<h3> ' + 'October' + ' ' + year + ' </h3>' ); break;
	case 10: $('.month-year').append('<h3> ' + 'November' + ' ' + year + ' </h3>' ); break;
	case 11: $('.month-year').append('<h3> ' + 'December' + ' ' + year + ' </h3>' ); break;
	default:
	break;

}


//Getting February Days Including The Leap Year
if (month == 1) {
  if ((year % 100!=0) && (year% 4==0) || (year%400 == 0 )) {
    febDays = 29;

  } else {
    febDays = 28;
  }

}


// Getting The Months and Days of the Week
var weekDayName = ["SUN", "MON", "TUES", "WED", "THURS", "FRI"];
var daysOfMonth = ["31", " " + febDays + " " ,"31", "30", "31", "30", "31", "31", "30", "31", "30", "31" ];


// Getting The Previous Month and the Next Month including the day of the week

var nextDay = nextMonth + ' 1 ,';
var weekDays = nextDay.day;
var weekDays2 = weekDays;
var numberDays = daysOfMonth;


// For the Previous Month
while (weekDays <= 0) {
  $('td').addClass('prevMonth').append(html);

// For the Next Loop
  weekDays--;
}

//To build the calendar body
while (counter <= numberDays) {

  //Staring a New Line
  if (weekDays2 > 6) {
      weekDays2 = 0;
      $('tr').append(html);
  }
}

// To see if Counter is the Current Day
// The current day is colored in

if(counter == day) {
  $('td').addClass('today').append( html + counter);

} else {
  $('td').addClass('currentMonth').append(html + counter);
}

weekDays2++;
counter++;

};

myCalendar();






//Navigation Buttons


$('.prev-month').click(function(){
	$('.month-year').empty();
	d.setUTCMonth(d.getUTCMonth() - 0);
	myCalendar();	
});

$('.next-month').click(function(){
	$('.month-year').empty();
	d.setUTCMonth(d.getUTCMonth() + 0);
	myCalendar();
});





});


